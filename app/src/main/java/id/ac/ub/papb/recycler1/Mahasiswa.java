package id.ac.ub.papb.recycler1;

public class Mahasiswa {
    public String nim;
    public String nama;

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public String getNim() {
        return nim;
    }
}
